<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return "HI this is a TEST PAGE!";
});

Route::get('/testroute', array("as" => "testroute", function() {
    return "This is a test route";
}));

//Route::get('/post/{id}', 'App\Http\Controllers\PostsController@index');

Route::resource('posts', 'App\Http\Controllers\PostsController');

Route::get('/contact', 'App\Http\Controllers\PostsController@contact');

Route::get('/showPost/{id}/{name}', '\App\Http\Controllers\PostsController@showPost');
